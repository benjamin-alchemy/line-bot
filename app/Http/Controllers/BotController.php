<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use LINE\LINEBot\HTTPClient\CurlHTTPClient;
use LINE\LINEBot;
use LINE\LINEBot\MessageBuilder\TextMessageBuilder;
use Log;

class BotController extends Controller
{
    public function index(Request $request)
    {
        $events = $request->input('events');
        $httpClient = new CurlHTTPClient('149SG0MRFHatGHebmqAxsUjbgT526oqVNCc8nDANq9YDwgoMQYBdg4MZxLrnw8y4wIjC40VH+o2px3i2BLubCezWq2nezRty9hKzjbg90gdEYYxHHjh7CBQuy4pgzTfPMw5ylZTTnC3znSvGLo91zwdB04t89/1O/w1cDnyilFU=');
        $bot = new LINEBot($httpClient, ['channelSecret' => '231514c2e5bf104d2c0324b678412528']);
        Log::info($events);

        foreach ($events as $event) {
            if (! isset($event['replyToken'])) continue;

            $textMessageBuilder = new TextMessageBuilder('hello');
            $response = $bot->replyMessage($event['replyToken'], $textMessageBuilder);
            Log::info(json_encode($response));
        }

        return response('ok', 200);
    }
}